#config
direction=1 # 1, -1
one_step=1000
full_steps=10
end_sensor_type=0 # 0-NO, 1-NC
speed=2
temperature_setpoint=20.0
temperature_offset=0.5
sleep_time=1 #minute

#app
import machine, uln2003, onewire, ds18x20, time, os

st=uln2003.Stepper(uln2003.HALF_STEP,12,13,14,10,speed)
led=machine.Pin(2, machine.Pin.OUT)
led.off() #onboard blue led
#time.sleep(5)

#deep sleep init
rtc = machine.RTC()
rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)
rtc.alarm(rtc.ALARM0, sleep_time*60*1000)

closed=machine.Pin(4, machine.Pin.IN)

def make_step(dir):
    if dir==-1:
        for i in range(one_step):
            if closed.value()==0^end_sensor_type:
                return -1
            else:
                st.step(1, dir*direction)
    else:
        st.step(one_step, dir*direction)
    return 0
    
#for i in range(full_steps):
#    if make_step(-1)==-1:
#        break
try:
    f = open('current_steps.txt')
    current_steps=int(f.read())
    f.close()
except:
    f = open('current_steps.txt', 'w')
    f.write("0")
    current_steps=0
    f.close()

ds_pin = machine.Pin(5)
ds_sensor = ds18x20.DS18X20(onewire.OneWire(ds_pin))
roms = ds_sensor.scan()
#print('Found DS devices: ', roms)
print("\n")
def getTemperature():
    roms = ds_sensor.scan()
    if len(roms) == 0:
        raise Exception("no sensor")
    else:
        ds_sensor.convert_temp()
        time.sleep_ms(750)
        return ds_sensor.read_temp(roms[0])

def update_current_steps():
    os.remove('current_steps.txt')
    f = open('current_steps.txt', 'w')
    f.write(str(current_steps))
    f.close()

try:
    t=getTemperature()
    print("Temperature: "+str(t))
    if abs(t-temperature_setpoint) > temperature_offset:
        if t-temperature_setpoint<0:
            make_step(-1)
            if closed.value()==0^end_sensor_type:
                if current_steps:
                    current_steps=0
                    update_current_steps()
            elif current_steps:
                current_steps=current_steps-1
                update_current_steps()           
        else:
            if current_steps<full_steps:
                current_steps=current_steps+1
                update_current_steps()
                make_step(1)
    print("Position: "+str(current_steps))
except Exception as inst:
    print(inst)
led.on()
machine.deepsleep()
#time.sleep(10)
